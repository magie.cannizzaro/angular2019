import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BeerService } from '../services/beer-search.service';
import { Beer } from '../models/Beer';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLoading = false;
  message = '';
  favouritesBeers: Beer[] = [];
  constructor(private service: BeerService) { }

  ngOnInit() {
    this.service.meObservable.subscribe((result => {
      this.favouritesBeers = result;
    }));
  }

  onFormSubmit(form: NgForm) {
    console.log('submit', form);
    this.isLoading = true;
    // controllo valori!!
    // invio al servizio


    setTimeout(() => {
      this.isLoading = false;
      this.message = 'Registrazione completata!';
    }, 1000 * 2);
  }

}
