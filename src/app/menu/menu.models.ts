export interface MenuItem {
  label: string;
  isActive?: boolean;
  icon?: string;
  badge?: number;
  link: string;
}
