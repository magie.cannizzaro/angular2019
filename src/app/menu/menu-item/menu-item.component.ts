import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from '../menu.models';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {

  @Input() menuItem: MenuItem;

  constructor() { }

  ngOnInit() {
  }

  getIcon(): string {
    if (this.menuItem.icon) {
      return this.menuItem.icon;
    } else {
      return 'home';
    }
  }

}
