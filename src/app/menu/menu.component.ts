import { Component, OnInit } from '@angular/core';
import { MenuItem } from './menu.models';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  menuItems: MenuItem[];
  constructor() { }

  ngOnInit() {
    console.log('Menu Component init!');
    this.createMenu();
  }


  createMenu() {
    console.log('create menu');

    this.menuItems = [
      {
        label: 'Home',
        isActive: true,
        badge: 99,
        link: '/home'

      },
      {
        label: 'Beers',
        icon: 'list',
        link: '/beers'
      },
      {
        label: 'Bookmarks',
        badge: 2,
        icon: 'envelope',
        link: '/bookmarks'
      }
    ];

    console.log('create menu end', this.menuItems);
  }

}
