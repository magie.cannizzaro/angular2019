import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Beer } from '../models/Beer';
import { storage } from '../utils/Storage';
import * as _ from 'lodash';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BeerService {

  private API_URL = 'https://api.openbrewerydb.org/breweries';

  public favouritesBeers: Beer[] = [];
  public meObservable: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient) {
    this.favouritesBeers = storage.getItem('favouritesBeers') || [];

    /*this.meObservable.subscribe((res) => {
      console.log('OB', res);
    });


    setTimeout(() => {
      this.meObservable.next(10);
    }, 1000);

    setTimeout(() => {
      this.meObservable.next(100);
    }, 2000);

    setTimeout(() => {
      this.meObservable.next(300);
    }, 3000);
    */
  }

  getSingleBeer(id: number) {
    return this.http.get(`${this.API_URL}/${id}`).toPromise();
  }

  searchBeers(searchTerm: string = '') {
    console.log('searchBeers', searchTerm);
    const param = {
      by_name: searchTerm
    };

    return this.http.get(this.API_URL, { params: param }).toPromise();
    /* .then(result => {
       console.log('result', result);
       return result;
     })
     .catch(error => {
       console.warn('error', error);
       return error;
     })
     .finally(() => {
       console.log('FINALLY!');
     });*/
  }


  getFavouritesBeers(): Beer[] {
    return this.favouritesBeers;
  }

  addToFavourites(newBeer: Beer) {
    this.favouritesBeers.push(newBeer);
    storage.setItem('favouritesBeers', this.favouritesBeers);
    console.log('favouritesBeers', this.favouritesBeers);

    this.meObservable.next(this.favouritesBeers);
  }

  removeFromFavourites(beerToRemove: Beer) {
    _.remove(this.favouritesBeers, (currentBeer, index) => {
      console.log(currentBeer, index);
      return currentBeer.id === beerToRemove.id;
    });
    storage.setItem('favouritesBeers', this.favouritesBeers);

    console.log('favouritesBeers', this.favouritesBeers);

    this.meObservable.next(this.favouritesBeers);
  }


}
