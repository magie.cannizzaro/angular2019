import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { OnSearchModel } from './on-search.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchTerm = '';
  @Output() onSearch = new EventEmitter<OnSearchModel>();

  @Input('showLoading') isLoading = false;
  constructor() { }

  ngOnInit() {
  }

  sendSearch(type: string) {
    console.log('sendSearch');
    this.onSearch.emit({
      type,
      term: this.searchTerm
    });
  }


}
