export interface OnSearchModel {
  term: string;
  type: string;
}
