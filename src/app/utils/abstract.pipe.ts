import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abstract'
})
export class AbstractPipe implements PipeTransform {

  transform(value: string, limit: number = 5, symbol: string = '...'): any {
    if (value.length > limit) {
      value = value.substr(0, limit);
    }
    return value + symbol;
  }

}
