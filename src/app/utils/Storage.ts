export const storage = {
  setItem: (key: string, obj: any) => {
    const value = JSON.stringify(obj);
    localStorage.setItem(key, value);
  },
  getItem: (key: string) => {
    const stringValue = localStorage.getItem(key);
    return JSON.parse(stringValue);
  }
};
