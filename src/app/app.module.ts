import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { TitleComponent } from './title-component/title.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SearchComponent } from './search/search.component';
import { MenuComponent } from './menu/menu.component';
import { ListComponent } from './list/list.component';
import { MenuItemComponent } from './menu/menu-item/menu-item.component';
import { ListItemComponent } from './list/list-item/list-item.component';
import { AbstractPipe } from './utils/abstract.pipe';
import { HomeComponent } from './home/home.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { BeersComponent } from './beers/beers.component';
import { BeerDetailComponent } from './beers/beer-detail/beer-detail.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,

  },
  {
    path: 'beers',
    component: BeersComponent,

  },
  {
    path: 'beers/:id',
    component: BeerDetailComponent,
    pathMatch: 'full'

  },
  {
    path: 'bookmarks',
    component: BookmarksComponent,

  },
  {
    path: '**',
    redirectTo: 'home'

  },

];

@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
    HeaderComponent,
    FooterComponent,
    SearchComponent,
    MenuComponent,
    ListComponent,
    MenuItemComponent,
    ListItemComponent,
    AbstractPipe,
    HomeComponent,
    BookmarksComponent,
    BeersComponent,
    BeerDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {
      enableTracing: true
    }),
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
