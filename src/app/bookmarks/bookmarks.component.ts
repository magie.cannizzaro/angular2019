import { Component, OnInit } from '@angular/core';
import { Beer } from "../models/Beer";
import { BeerService } from '../services/beer-search.service';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss']
})
export class BookmarksComponent implements OnInit {
  favouritesBeers: Beer[] = [];

  constructor(private service: BeerService) { }

  ngOnInit() {
    this.favouritesBeers = this.service.getFavouritesBeers();
  }

}
