import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BeerService } from 'src/app/services/beer-search.service';
import { Beer } from '../../models/Beer';

@Component({
  selector: 'app-beer-detail',
  templateUrl: './beer-detail.component.html',
  styleUrls: ['./beer-detail.component.scss']
})
export class BeerDetailComponent implements OnInit {

  currentBeerId: number;
  currentBeer: Beer;

  constructor(private route: ActivatedRoute, private service: BeerService) { }

  ngOnInit() {
    console.log('ACTIVATE PARAMS', this.route);

    this.route.params.subscribe((result) => {
      console.log(result);
      this.currentBeerId = result.id;
      this.getBeer();
    });
  }


  getBeer() {
    this.service.getSingleBeer(this.currentBeerId).then((result: Beer) => {
      this.currentBeer = result;
    });
  }

}
