import { Component, OnInit, Input } from '@angular/core';
import { Beer } from '../models/Beer';
import { OnSearchModel } from '../search/on-search.model';
import { BeerService } from '../services/beer-search.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-beers',
  templateUrl: './beers.component.html',
  styleUrls: ['./beers.component.scss']
})
export class BeersComponent implements OnInit {

  isLoading: boolean;
  searchTerm: string;

  beersArray: Beer[] = [];
  favouritesBeers: Beer[] = [];

  constructor(private service: BeerService) { }

  ngOnInit() {
    this.favouritesBeers = this.service.favouritesBeers;

    this.searchBeers();
  }


  onClickedSearch(e: OnSearchModel) {
    this.isLoading = true;
    console.log('onClickSearch event=', e);
    this.searchTerm = e.term;

    console.log('onClickSearch', this.searchTerm);

    this.searchBeers(this.searchTerm);
  }

  searchBeers(searchTerm: string = '') {
    const beers = this.service.searchBeers(searchTerm)
      .then((result: Beer[]) => {
        console.log('result', result);

        this.isLoading = false;
        this.beersArray = this.filterResults(result);
      });
  }

  filterResults(beers: Beer[]): Beer[] {
    if (this.favouritesBeers.length) {
      beers = beers.map((beer, index) => {
        const isFav = _.find(this.favouritesBeers, (fav, x) => {
          return fav.id === beer.id;
        });
        if (isFav) {
          beer.liked = true;
        }
        return beer;
      });
    }
    return beers;
  }

}
