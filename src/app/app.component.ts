import { Component, OnInit } from '@angular/core';
import { BeerService } from './services/beer-search.service';
import { Beer } from './models/Beer';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  favouritesBeers: Beer[] = [];


  constructor(private service: BeerService) { }

  ngOnInit() {

    this.favouritesBeers = this.service.favouritesBeers;

    this.service.meObservable.subscribe((result => {
      console.log('Novità!', result);
      this.favouritesBeers = result;
    }));
  }

}
