import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title',
  template: '<h2>{{titolo}}</h2>',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent {
  @Input() titolo = 'title property';
}
