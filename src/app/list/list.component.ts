import { Component, OnInit, Input } from '@angular/core';
import { Beer } from '../models/Beer';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() beersArray: Beer[] = [];
  @Input() isLoading = false;

  constructor() { }

  ngOnInit() {
  }

}
