import { Component, OnInit, Input } from '@angular/core';
import { Beer } from '../../models/Beer';
import { BeerService } from 'src/app/services/beer-search.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input() beer: Beer;

  constructor(private service: BeerService, private router: Router) { }

  ngOnInit() {
  }

  goDetail(e: MouseEvent) {
    console.log('goDetail');
    this.router.navigate(['beers/' + this.beer.id]);
  }
  onClickHeart(e: MouseEvent) {
    console.log('onClickItem', e.currentTarget);
    this.beer.liked = !this.beer.liked;

    if (this.beer.liked) {
      this.service.addToFavourites(this.beer);
    } else {
      this.service.removeFromFavourites(this.beer);
    }

    console.log('favouritesBeers', this.service.favouritesBeers);
    e.preventDefault();
    e.stopPropagation();
  }

}
